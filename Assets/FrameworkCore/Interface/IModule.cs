﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 模块的接口
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 模块的接口
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// 模块名
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 添加模块内管理器
        /// </summary>
        /// <param name="mgr"></param>
        void AddManager(BaseManager mgr);

        /// <summary>
        /// 添加本模块控制器
        /// </summary>
        /// <param name="ctrl"></param>
        void AddController(BaseController ctrl);

        /// <summary>
        /// 移除本模块管理器
        /// </summary>
        /// <param name="mgr"></param>
        void RemoveManager(BaseManager mgr);

        /// <summary>
        /// 移除本模块控制器
        /// </summary>
        /// <param name="ctrl"></param>
        void RemoveController(BaseController ctrl);

        /// <summary>
        /// 清理本模块自身
        /// </summary>
        void ClearSelf();
    }
}
